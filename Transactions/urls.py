from . import views
from django.urls import path

app_name = 'Transactions'

urlpatterns = [
    path('Transaction_Add/',views.Transaction_Add,name='transaction_add'),
    path('Add_Source/',views.add_source,name='add_source'),
    path('settings/',views.Settings,name='setting1'),
    path(r'^Delete_Source/(?P<pk>)/$', views.delete_source, name='source_delete')
]

