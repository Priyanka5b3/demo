# Generated by Django 3.0.4 on 2020-08-18 07:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Transactions', '0002_auto_20200817_1900'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transactions',
            name='transaction_type',
            field=models.CharField(max_length=8),
        ),
    ]
