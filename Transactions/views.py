from django.shortcuts import render,redirect
from .models import Transactions,Balance_Amount,Source
from .forms import Transaction_Form,Source_Add

def Transaction_Add(request):
    if request.method == "POST":
        form1 = Transaction_Form(request.POST)
        if form1.is_valid():
            amount = form1.cleaned_data.get('Amount')
            exist_object = Balance_Amount.objects.get(id=1)
            exist_balance = exist.Balance
            if exist_balance-amount < 0:
                return HttpResponse("Don't have enough balance to deduce the transaction")
            else:
                form1.save()
                exist_balance = exist_balance+amount
                exist_balance.save()
                return HttpResponse("<h1>Transaction has been successfully added</h1>")
    else:
        form1 = Transaction_Form()
        return render(request,'Transactions/Add_Transactions.html',{'form1':form1})

def add_source(request):
    f1=Source_Add()
    if request.method == "POST":
        f1=Source_Add(request.POST)
        if f1.is_valid():
            f1.save()
            return redirect('Transactions:add_source')
        else:
            return HttpResponse("<h1>The Form is having issues </h1>")
    else:
        c1=Source.objects.values_list('source_name').values
        return render(request,'Transactions/Add_Source.html',{'form':f1,'objects':c1})

def delete_source(request,pk):
        c1=Source.objects.get(id=pk)
        c1.delete()
        return redirect('Transactions:add_source')



def Settings(request):
    return render(request,'Transactions/settings.html')

