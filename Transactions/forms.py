from .models import Transactions, Source
from django import forms




class Transaction_Form(forms.ModelForm):
    choice = (
        ('Cred', "Credit"),
        ('Deb', "debit"),
    )
    transaction_type = forms.ChoiceField(widget=forms.Select(),choices=choice,
                                         required=True)
    transaction_source = forms.CharField(max_length=20)
    class Meta:
        model = Transactions

        fields = ['transaction_source', 'Amount', 'description', 'pdf', 'date','transaction_type']


class Source_Add(forms.ModelForm):
    class Meta:
        model = Source
        fields = ('source_name',)
