from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Source(models.Model):
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    source_name = models.CharField(max_length=20)


class Balance_Amount(models.Model):
    owner = models.OneToOneField(User, on_delete=models.CASCADE)
    balance = models.IntegerField()
    date = models.DateField(auto_now_add=True)

choice = (
    ('Cred', "Credit"),
    ('Deb', "debit"),
)


class Transactions(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='transactions')
    transaction_type = models.CharField(max_length=8)
    transaction_source = models.ForeignKey(Source, on_delete=models.CASCADE)
    Amount = models.IntegerField()
    transaction_id = models.AutoField(primary_key = True)
    description = models.CharField(max_length = 10)
    pdf = models.ImageField(upload_to='uploads/')
    date = models.DateField()




