from django.shortcuts import render
from .forms import UserRegisterForm
from django.http import HttpResponse
from django.contrib.auth import login,authenticate
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm

# Create your views here.

def home_page(request):
    return render(request,'Authentication/base.html')



def register(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            email=form.cleaned_data.get('email')
            html_data = get_template('Authentication/Email.html')
            d = {'username':username}
            subject,from_email,to = 'Welcome','priyanka.vangapandu@gmail.com',email
            html_content = html_data.render(d)
            msg=EmailMultiAlternatives(subject,html_content,from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            #################################
            messages.success(request,f'Your account has been created')
            return HttpResponse("<h1> successfully logged in </h1>")
        else:
            return HttpResponse("<h1>The form is invalid</h1>")

    else:
        form = UserRegisterForm()
        return render(request,'Authentication/register.html',{'form':form,'title':"register here"})


def Login_Account(request):
    if request.method == "POST":
        username=request.POST['username']
        password = request.POST['password']
        user=authenticate(request,username=username,password=password)
        if user is not None:
            form = login(request,user)
            messages.success(request,f'welcome {username}!!')
            return render(request,'Authentication/base.html')
        else:
            messages.info(request,f'Accounts do not exist then please try again later')
    else:
        form = AuthenticationForm()
        return render(request,'Authentication/login.html',{'form':form,'title':'log in'})










