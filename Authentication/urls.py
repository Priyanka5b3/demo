from django.http import HttpResponse
from django.urls import path
from . import views
from django.contrib.auth import views as auth

app_name = 'Authentication'
urlpatterns = [
    path('SignUp/', views.register, name='Register'),
    path('Login/', views.Login_Account, name='Login'),
    path('Logout/', auth.LogoutView.as_view(template_name='Authentication/base.html'), name='Logout'),
]